package com.target.processor;

import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.target.beans.IndexBean;
import com.target.util.SearchUtils;

public class SearchProcessorTest {
	Map<String, Long> results;
	static SearchProcessor searchProcessor; 
	static String[] paths = { "data/input/french_armed_forces.txt",
			"data/input/hitchhikers.txt", "data/input/warp_drive.txt"};
	
	static IndexBean idx = null;
	static String indexPath = "data/index";
	
	@BeforeClass
	public static void before() {
		searchProcessor = new SearchProcessor();
		idx = new IndexBean();
		searchProcessor.setIdxBean(idx);
		System.out.println("End of beforeMethod");
	}
	
	@Test
	public void testSearch() {
		results = searchProcessor.search(paths, "it", "1");
		SearchUtils.printMap(results);
		Assert.assertTrue(results.size() > 0);
	}

	@Test
	public void testSearchRegex() {
		results = searchProcessor.search(paths, "it", "2");
		SearchUtils.printMap(results);
		Assert.assertTrue(results.size() > 0);
	}
	
	@Test
	public void testSearchWithIndex() {
		results = searchProcessor.search("it");
		SearchUtils.printMap(results);
		Assert.assertTrue(results.size() > 0);
	}
}
