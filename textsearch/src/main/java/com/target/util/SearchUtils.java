package com.target.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.target.exceptions.DefaultSearchException;

/**
 * The utility class.
 *
 */
public class SearchUtils {

	private static Logger logger = LoggerFactory.getLogger(SearchUtils.class);
	private static Properties properies;
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		Map<K, V> result = null;
		if(MapUtils.isNotEmpty(map)) {
			List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
				public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
					return (o2.getValue()).compareTo(o1.getValue());
				}
			});
			result = new LinkedHashMap<K, V>();
			for (Map.Entry<K, V> entry : list) {
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}
	
	public static <K, V> void printMap(Map<K, V> map) {
		if(MapUtils.isNotEmpty(map)) {
			for (Map.Entry<K, V> entry : map.entrySet()) {
				System.out.println("   " + entry.getKey() + " - " + entry.getValue() + " matches");
			}
		}
	}

	/**
	 * Regular string search.
	 * 
	 * @param currentLine
	 * @param searchTerm
	 * @param p
	 * @return
	 */
	public static long matchString(StringBuilder currentLine, String searchTerm, Pattern p) {
		int index = 0;
		long matches = 0;
		int length = searchTerm.length();
		while (index < currentLine.length()) {
			int currentIndex = currentLine.indexOf(searchTerm, index) ;
			if (currentIndex >= 0) {
				boolean isWord = currentIndex == 0 ? (currentLine.substring(currentIndex + length).startsWith(" ") || currentIndex + length == currentLine.length()) 
						: p.matcher(currentLine.substring(currentIndex-1, currentIndex)).find() 
						&& (currentIndex + length == currentLine.length() || p.matcher(currentLine.substring(currentIndex + length, currentIndex + length + 1)).find()); 
				if(isWord) {
				   matches++;
				   index = currentLine.indexOf(searchTerm, index);
				}
			} 
			index++;
		}
		return matches;
	}

	/**
	 * Regex search.
	 * 
	 * @param currentLine
	 * @param pattern
	 * @return
	 */
	public static long matchRegex(StringBuilder currentLine, Pattern pattern) {
		long matches = 0;
		Matcher matcher = pattern.matcher(currentLine);
		int from = 0;
		while (matcher.find(from)) {
			matches++;
			from = matcher.start() + 1;
		}
		return matches;
	}
	
	static {
		properies = new Properties();
		ClassLoader classLoader = null;
		InputStream inputStream = null;
		try {
			classLoader = Thread.currentThread().getContextClassLoader();
			inputStream = classLoader.getResourceAsStream("application.properties");
			properies.load(inputStream);
		} catch (IOException io) {
			logger.error("IO Exception occured while loading the properties file" + io);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					logger.error("IO Exception occured while loading the properties file" + e);
				}
			}

		}
	}
	
	public static String getProperty(String key, String defaultValue) {
		
		if (null != properies)
			return properies.getProperty(key, defaultValue);
		else
			throw new DefaultSearchException("Exception occured while getting the properties from the file");
	}
	
	public static String getProperty(String key) {
		
		if (null != properies)
			return properies.getProperty(key);
		else
			throw new DefaultSearchException("Exception occured while getting the properties from the file");
	}
}
