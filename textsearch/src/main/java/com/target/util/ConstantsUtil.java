package com.target.util;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * Constants class.
 */
public final class ConstantsUtil {

	public static final String SEARCH_PATH = "search.file.path";

	public static final String IDX_PATH = "search.index.path";

	public static final String COMMA_DELIM = ",";

	public static final String SEARCH_DOCS = "search.doc.dir";

	public static final String SPECIAL_CHAR_BEG = "^[^a-zA-Z0-9]+";

	public static final String SPECIAL_CHAR_END = "[^a-zA-Z0-9]+$";

	public static final String SPECIAL_CHAR_REGEX = "[^A-Za-z0-9]";

	public static final String REGEX_WORD_BOUND = "\\b";

	public static final String QUERY_PARSER_ESC = "\"";
	
	public static final String by_string = "1";
	
	public static final String by_regex = "2";
	
	public static final String by_index = "3";
	
	public static final String IDX_FIELD_CONTENT = "contents";
	
	public static final String IDX_FIELD_MOD = "modified";
	
	public static final String IDX_FIELD_PATH = "path";
	
	public static final List<String> IDX_STOP_WORDS = (List<String>) Arrays.asList("a","an");
	
	public static final String IDX_PHRASE_FREQ = "= phraseFreq";
	
	public static final String IDX_TERM_FREQ = "= termFreq";
	
	public static final String IDX_FREQ = "freq=";

	public static final String IDX_PATH_DEF = "data/index";

	public static final String SEARCH_PATH_DEF = "data/input/french_armed_forces.txt,data/input/hitchhikers.txt,data/input/warp_drive.txt";
	
	public static final String SEARCH_DOCS_DEF = "data/input";
	
	public static final ExecutorService exec_pool = Executors.newFixedThreadPool(3);
	
}
