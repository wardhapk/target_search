package com.target.beans;

import static com.target.util.ConstantsUtil.IDX_FIELD_CONTENT;
import static com.target.util.ConstantsUtil.IDX_PATH;
import static com.target.util.ConstantsUtil.IDX_PATH_DEF;
import static com.target.util.ConstantsUtil.IDX_STOP_WORDS;

import java.io.IOException;
import java.nio.file.Paths;

import javax.inject.Named;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.target.exceptions.DefaultSearchException;
import com.target.util.SearchUtils;

@Named
public class IndexBean {
	
	private static Logger logger = LoggerFactory.getLogger(IndexBean.class);
	
	private IndexReader reader;

	private IndexSearcher searcher;

	private Analyzer analyzer;

	private QueryParser parser;

	private final CharArraySet charArraySet = new CharArraySet(IDX_STOP_WORDS, true);
	
	private String indexPath = null;
	
	public IndexBean() {
		try {
			indexPath = SearchUtils.getProperty(IDX_PATH, IDX_PATH_DEF);
			this.reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPath)));
			this.searcher = new IndexSearcher(reader); 
			this.analyzer = new StandardAnalyzer(charArraySet);
			this.parser = new QueryParser(IDX_FIELD_CONTENT, analyzer);
		} catch (Exception e) {
			logger.error("Exception occured during initialization. \n" + e);
			throw new DefaultSearchException(e.getMessage(), e.getCause());
		}
	}

	public IndexBean(final IndexReader reader, final IndexSearcher searcher, 
			final Analyzer analyzer, final QueryParser parser) {
		this.reader = reader;
		this.searcher = searcher; 
		this.analyzer = analyzer;
		this.parser = parser;
		
	}
	
	/**
	 * @return the reader
	 */
	public IndexReader getReader() {
		return reader;
	}

	/**
	 * @param reader the reader to set
	 */
	public void setReader(IndexReader reader) {
		this.reader = reader;
	}

	/**
	 * @return the searcher
	 */
	public IndexSearcher getSearcher() {
		return searcher;
	}

	/**
	 * @param searcher the searcher to set
	 */
	public void setSearcher(IndexSearcher searcher) {
		this.searcher = searcher;
	}

	/**
	 * @return the analyzer
	 */
	public Analyzer getAnalyzer() {
		return analyzer;
	}

	/**
	 * @param analyzer the analyzer to set
	 */
	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	/**
	 * @return the parser
	 */
	public QueryParser getParser() {
		return parser;
	}

	/**
	 * @param parser the parser to set
	 */
	public void setParser(QueryParser parser) {
		this.parser = parser;
	}

	public void postprocess() {
		try {
			this.reader.close();
		} catch (IOException e) {
			logger.error("Exception occured in the postprocess method" + e);
		}
	}
		
}
