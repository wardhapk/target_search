package com.target.exceptions;

public class DefaultSearchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DefaultSearchException() {
		super();
	}

	public DefaultSearchException(String message, Throwable cause) {
		super(message, cause);
	}

	public DefaultSearchException(String message) {
		super(message);
	}
}
