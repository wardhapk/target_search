package com.target.search;

import static com.target.util.ConstantsUtil.IDX_FIELD_CONTENT;
import static com.target.util.ConstantsUtil.IDX_FIELD_MOD;
import static com.target.util.ConstantsUtil.IDX_FIELD_PATH;
import static com.target.util.ConstantsUtil.IDX_PATH;
import static com.target.util.ConstantsUtil.IDX_PATH_DEF;
import static com.target.util.ConstantsUtil.IDX_STOP_WORDS;
import static com.target.util.ConstantsUtil.SEARCH_DOCS;
import static com.target.util.ConstantsUtil.SEARCH_DOCS_DEF;
import static com.target.util.SearchUtils.getProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;;


/**
 * IndexRunner class has the various methods 
 * which performs the indexing process.
 *
 */
public class IndexRunner implements Indexer {

	private static Logger logger = LoggerFactory.getLogger(IndexRunner.class);
	
	public IndexRunner() {
	}

	/**
	 * Index all text files under a directory.
	 * 
	 * @param mode
	 */
	@Override
	public void index() {

		IndexWriter writer = null;
		Directory dir = null;
		try {
			String indexPath = getProperty(IDX_PATH, IDX_PATH_DEF);
			Path docDir = Paths.get(getProperty(SEARCH_DOCS, SEARCH_DOCS_DEF));
			if (!Files.isReadable(docDir)) {
				logger.info("Document directory '" + docDir.toAbsolutePath() + "' does not exist or is not readable, please check the path");
				System.exit(1);
			}
			logger.info("Indexing to directory '" + indexPath + "'...");
			if( StringUtils.isNotEmpty(indexPath)) {
			   dir = FSDirectory.open(Paths.get(indexPath));
			} else {
				logger.error("Index path is empty, please retrigger indexing..");
				System.exit(1);
			}
			CharArraySet charArraySet = new CharArraySet(IDX_STOP_WORDS, true);
			Analyzer analyzer = new StandardAnalyzer(charArraySet);
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			writer = new IndexWriter(dir, iwc);
			indexDocs(writer, docDir);
		} catch (Exception e) {
			logger.error(" Indexing failed, caught a " + e.getClass() + "\n with message: " + e.getMessage());
			System.exit(1);
		} finally {
			if(null != writer) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error("Exception occured while closing the IndexWriter.." + e.getStackTrace());
				}
			}
			logger.info("Indexing completed successfully..");
		}
	}

	/**
	 * Index the docs under the given index directory.
	 * 
	 * @param writer
	 * @param path
	 * @throws IOException
	 */
	private void indexDocs(final IndexWriter writer, Path path)
			throws IOException {
		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file,
						BasicFileAttributes attrs) throws IOException {
					try {
						indexDoc(writer, file, attrs.lastModifiedTime()
								.toMillis());
					} catch (IOException io) {
						logger.error("IO Exception occured while reading the file:: " + file.getFileName() + io);
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
		}
	}

	/**
	 * Indexes a single document
	 * 
	 * @param writer
	 * @param file
	 * @param lastModified
	 * @throws IOException
	 */
	private void indexDoc(IndexWriter writer, Path file, long lastModified)
			throws IOException {
		
		FieldType type = null;
		Document doc = null;
		Field pathField = null;
		try (InputStream stream = Files.newInputStream(file)) {
			doc = new Document();
			
			pathField = new StringField(IDX_FIELD_PATH, file.toString(), Field.Store.YES);
			doc.add(pathField);
			
			doc.add(new LongPoint(IDX_FIELD_MOD, lastModified));
			
			type = new FieldType(); 
			type.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
			type.setStoreTermVectors(true);
	        doc.add(new Field(IDX_FIELD_CONTENT, new BufferedReader(new InputStreamReader(stream)), type));
			
			if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
				logger.debug("adding " + file);
				writer.addDocument(doc);
			} else {
				logger.debug("updating " + file);
				writer.updateDocument(new Term("path", file.toString()), doc);
			}
		}
	}
	
	public static void main (String args[]) {
		IndexRunner idx = new IndexRunner();
		idx.index();
	}
}