package com.target.search;

import static com.target.util.ConstantsUtil.COMMA_DELIM;
import static com.target.util.ConstantsUtil.SEARCH_PATH;
import static com.target.util.ConstantsUtil.SEARCH_PATH_DEF;
import static com.target.util.ConstantsUtil.by_index;
import static com.target.util.ConstantsUtil.by_regex;
import static com.target.util.ConstantsUtil.by_string;
import static com.target.util.ConstantsUtil.exec_pool;

import java.util.Map;
import java.util.Scanner;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.target.exceptions.DefaultSearchException;
import com.target.processor.SearchProcessor;
import com.target.util.SearchUtils;

/**
 * The SearchRunner class executes the search by taking
 * the input search term and the input search method.
 *
 */
@Named
public class SearchRunner {

	private String[] filePath = null;
	
	@Inject
	private SearchProcessor searchProcessor;
	
	private static AnnotationConfigApplicationContext ctx = null;
	
	private static Logger logger = LoggerFactory.getLogger(SearchRunner.class);
	
	private Scanner scanner = null;
	
	public static void main(String args[]) {
		logger.info("Started search process..");
		initCtx();   
		SearchRunner runner = ctx.getBean(SearchRunner.class);
		runner.validate();
		runner.dosearch();
		logger.info("End of search process..");
	}

	public void dosearch() {

		scanner = new Scanner(System.in);
		boolean exit = false;
		while (!exit) {
			System.out.println("Enter the search term: ");
			String searchTerm = scanner.nextLine();
			if (StringUtils.isEmpty(searchTerm) || StringUtils.isBlank(searchTerm)) {
				System.out.println("Invalid search term. Exiting..");
				System.exit(0);
			}
			System.out.println("Enter search Method: 1) String Match 2) Regular Expression 3) Indexed");
			String searchMethod = scanner.nextLine();

			Map<String, Long> results = null;
			StopWatch sw = new StopWatch();
			sw.reset();
			sw.start();
			
			switch (searchMethod) {
			case by_index:
				results = searchProcessor.search(searchTerm);				
				break;
			case by_regex:
			case by_string: {
				results = searchProcessor.search(filePath, searchTerm, searchMethod);				
				break;
			}
			default:
				System.out.println("Invalid option.");
				break;
			}
			sw.stop();
			
			if(MapUtils.isNotEmpty(results)) {
				System.out.println("Search Results: ");
				SearchUtils.printMap(SearchUtils.sortByValue(results));			
				System.out.println("Elapsed Time: " + sw.toString() + "ms");
			} else {
				System.out.println("Search returned no results!");
			}
			
			System.out.println("\nPress Enter to continue..");
			String enter = scanner.nextLine();
			exit = StringUtils.isNotEmpty(enter);
			if (exit) {
				System.out.println("Exiting..");
                this.teardown();
				System.exit(0);
			}
		}
	}

	private void validate() {
		try {
			filePath = SearchUtils.getProperty(SEARCH_PATH, SEARCH_PATH_DEF).split(COMMA_DELIM);
		} catch (DefaultSearchException e) {
			logger.error("Exception occured while validating the search/index paths. Exiting.." + e.getStackTrace());
			System.exit(1);
		}
	}
	
	private static void initCtx() {
		try {
			logger.info("Initializing spring context..");
			ctx = new AnnotationConfigApplicationContext();
			ctx.scan("com.target");
			ctx.refresh();
			logger.info("Context initialized successfully..");
		} catch (Throwable t) {
			logger.error("Exception occured while context initialization.." + t);
			System.exit(1);
		}
	}
	
	private void teardown(){
		searchProcessor.getIdxBean().postprocess();
		exec_pool.shutdown();
		scanner.close();
	}
	
	/**
	 * @return the filePath
	 */
	public String[] getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String[] filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the searchProcessor
	 */
	public SearchProcessor getSearchProcessor() {
		return searchProcessor;
	}

	/**
	 * @param searchProcessor the searchProcessor to set
	 */
	public void setSearchProcessor(SearchProcessor searchProcessor) {
		this.searchProcessor = searchProcessor;
	}
}
