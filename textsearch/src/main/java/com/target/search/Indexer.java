package com.target.search;

/**
 * The Indexer interface.
 *
 */
public interface Indexer {
	void index();
}
