package com.target.processor;

import static com.target.util.ConstantsUtil.IDX_FIELD_PATH;
import static com.target.util.ConstantsUtil.IDX_FREQ;
import static com.target.util.ConstantsUtil.IDX_PHRASE_FREQ;
import static com.target.util.ConstantsUtil.IDX_TERM_FREQ;
import static com.target.util.ConstantsUtil.QUERY_PARSER_ESC;
import static com.target.util.ConstantsUtil.REGEX_WORD_BOUND;
import static com.target.util.ConstantsUtil.SPECIAL_CHAR_BEG;
import static com.target.util.ConstantsUtil.SPECIAL_CHAR_END;
import static com.target.util.ConstantsUtil.SPECIAL_CHAR_REGEX;
import static com.target.util.ConstantsUtil.by_regex;
import static com.target.util.ConstantsUtil.by_string;
import static com.target.util.ConstantsUtil.exec_pool;
import static com.target.util.SearchUtils.matchRegex;
import static com.target.util.SearchUtils.matchString;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.target.beans.IndexBean;
import com.target.exceptions.DefaultSearchException;

/**
 * The class which has all the various 
 * methods for each of the search types.
 *
 */
@Named
public class SearchProcessor {
	
	private static Logger logger = LoggerFactory.getLogger(SearchProcessor.class);
	
	@Inject
	private IndexBean idxBean;
	
	/**
	 * default constructor
	 */
	public SearchProcessor(){
		
	}
	
    /**
     * Search by string matching and regular expression.
     * 
     * @param filePath - list of files paths which need to be searched.
     * @param searchTerm - the search term.
     * @param searchMethod - the search method type.
     * @return
     */
    public Map<String, Long> search(String[] filePath, String searchTerm, String searchMethod) {
		
    	String termFilter = searchTerm.replaceAll(SPECIAL_CHAR_BEG, StringUtils.EMPTY).replaceAll(SPECIAL_CHAR_END, StringUtils.EMPTY).toUpperCase();
		Map<String, Long> results = new HashMap<>();
		Pattern patternStr = Pattern.compile(SPECIAL_CHAR_REGEX);
		Pattern patternReg = Pattern.compile(REGEX_WORD_BOUND + Pattern.quote(termFilter) + REGEX_WORD_BOUND);
		List<Callable<Map.Entry<String, Long>>> callables = null;
			callables = new ArrayList<>();
        try {
			for (String path: filePath) {
				
				Callable<Map.Entry<String, Long>> task = () -> {
					BufferedReader bufferedReader = null;
					long matches = 0;
					try {
						FileInputStream fis = new FileInputStream(path.trim());
						bufferedReader = new BufferedReader(new InputStreamReader(fis));
						String currentLine;
						while ((currentLine = bufferedReader.readLine()) != null) {
							switch (searchMethod) {
							case by_string:
								matches = matches + matchString(new StringBuilder(currentLine.toUpperCase()), termFilter, patternStr);
								break;
							case by_regex:
								matches = matches + matchRegex(new StringBuilder(currentLine.toUpperCase()), patternReg);
								break;
						    }
						}
					} catch (Exception e) {
						logger.error("Exception occured in the search method." + e);
						throw new DefaultSearchException(e.getMessage(), e.getCause());
					} finally {
						try {
							if(null != bufferedReader) {
								bufferedReader.close();	
							}
						} catch (IOException e) {
							logger.error("Exception occured while closing the reader!" + e);
						}
					}
					return new AbstractMap.SimpleEntry<String, Long>(path, matches);
				};
				callables.add(task);
			}
			if(CollectionUtils.isNotEmpty(callables)) {
				exec_pool.invokeAll(callables).forEach( future -> {
					try {
						results.put(future.get().getKey(), future.get().getValue());
					} catch (InterruptedException | ExecutionException e) {
						logger.error("Exception occured in the search method." + e);
						throw new DefaultSearchException(e.getMessage(), e.getCause());
					}
				});
			}
        } catch(InterruptedException e) {
        	logger.error("Exception occured in the search method." + e);
			throw new DefaultSearchException(e.getMessage(), e.getCause());
        }
		return results;
	}
	
	/**
	 * Search the input token in the previously indexed files.
	 * 
	 * @param searchTerm - the search term
	 * @return
	 */
	public Map<String, Long> search(String searchTerm) {
		
		Map<String, Long> results = new HashMap<String, Long>();
		if (null == idxBean) return results; 
		List<Callable<Map.Entry<String, Long>>> callables = null;
		try {
			Query query = idxBean.getParser().parse(QUERY_PARSER_ESC + QueryParser.escape(searchTerm).toUpperCase() + QUERY_PARSER_ESC);
			callables = new ArrayList<>();
			for (int i =0; i < idxBean.getReader().maxDoc() ; i++) {
				int docval = i;
				
				Callable<Map.Entry<String, Long>> task = () -> {
					long termFreq = 0;
					Document doc = idxBean.getSearcher().doc(docval); 
					Explanation explanation =  idxBean.getSearcher().explain(query, docval); 
					if(ArrayUtils.isNotEmpty(explanation.getDetails())) {
					   String desc = (explanation.getDetails()[0]).getDescription();
					   termFreq = desc.contains(IDX_PHRASE_FREQ) ? (long) Double.parseDouble(desc.substring(desc.indexOf(IDX_FREQ)+5, 
							   desc.indexOf(IDX_PHRASE_FREQ)).trim()) : (long) Double.parseDouble(desc.substring(desc.indexOf(IDX_FREQ)+5, 
									   desc.indexOf(IDX_TERM_FREQ)).trim()) ;
					}
					return new AbstractMap.SimpleEntry<String, Long>(doc.get(IDX_FIELD_PATH), termFreq);
				};
				
				callables.add(task);
			}
			if(CollectionUtils.isNotEmpty(callables)) {
				exec_pool.invokeAll(callables).forEach( future -> {
					try {
						results.put(future.get().getKey(), future.get().getValue());
					} catch (InterruptedException | ExecutionException e) {
						logger.error("Exception occured in the search method." + e);
						throw new DefaultSearchException(e.getMessage(), e.getCause());
					}
				});
			}
			
		} catch (InterruptedException | ParseException e) {
			logger.error("Exception occured in the search by index method" + e);
			throw new DefaultSearchException(e.getMessage(), e.getCause());
		}
		return results;
	}

	/**
	 * @return the idxBean
	 */
	public IndexBean getIdxBean() {
		return idxBean;
	}

	/**
	 * @param idxBean the idxBean to set
	 */
	public void setIdxBean(IndexBean idxBean) {
		this.idxBean = idxBean;
	}
}
